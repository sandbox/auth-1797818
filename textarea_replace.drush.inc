<?php

/**
 * Implementation of hook_drush_command().
 */
function textarea_replace_drush_command() {
  $items['textarea-replace'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'search' => 'Search',
      'replace' => 'Replace',
    ),
  );
  return $items;
}

function drush_textarea_replace($search=NULL, $replace=NULL) {
  if(is_null($search)) return drush_set_error('TEXTAREA_REPLACE_ARGUMENT_MISSING', 'No search given');
  if(is_null($replace)) return drush_set_error('TEXTAREA_REPLACE_ARGUMENT_MISSING', 'No replace given');

  if(function_exists('field_info_fields')) {
    drush_textarea_replace_d7($search, $replace);
  } else {
    drush_textarea_replace_d6($search, $replace);
  }

  drupal_flush_all_caches();
  return drush_log("Done, all occurensies of '$search' has been replaced by '$replace'");
}

function drush_textarea_replace_d6($search=NULL, $replace=NULL) {
  // update all node body and teaser fields
  db_query("UPDATE {node_revisions} SET body=REPLACE(body, '%s', '%s'), teaser=REPLACE(teaser, '%s', '%s');", $search, $replace, $search, $replace);
  // Update all fields of type text
  if(module_exists('content')) {
    $fields = content_fields();
    foreach($fields as $field) {
      if($field['type'] != 'text') continue;
      $db_info = content_database_info($field);
      if(!empty($db_info['table']) && !empty($db_info['columns']['value']['column'])) {
        $table = $db_info['table'];
        $column = $db_info['columns']['value']['column'];
        db_query("UPDATE {$table} SET $column = REPLACE($column, '%s','%s');", $search, $replace);
      }
    }
  }
}

function drush_textarea_replace_d7($search=NULL, $replace=NULL) {
  $fields = field_info_fields();
  foreach($fields as $field) {
    if(
      !in_array($field['type'], array('text_long', 'text_with_summary')) ||
      $field['storage']['type'] != 'field_sql_storage'
    ) continue;
    foreach($field['storage']['details']['sql'] as $value) {
      $table = reset(array_keys($value));
      foreach($value[$table] as $column_key => $column) {
        if($column_key == 'format') continue;
        db_query("UPDATE {$table} SET $column = REPLACE($column, :search, :replace);", array(':search' => $search, ':replace' => $replace));
      }
    }
  }
}

